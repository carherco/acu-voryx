api_protected_with_oauth
========================

symfony new api_protected_with_oauth 2.7
cd api_protected_with_oauth

composer.phar require bshaffer/oauth2-server-bundle --ignore-platform-reqs
composer.phat require voryx/restgeneratorbundle --ignore-platform-reqs
composer.phar require nelmio/cors-bundle --ignore-platform-reqs
composer.phar update --ignore-platform-reqs


En routing.yml

    oauth2_server:
        resource: "@OAuth2ServerBundle/Controller/"
        type:     annotation
        prefix:   /

Configuración voryx:

        framework:
            csrf_protection: false #only use for public API
        
        fos_rest:
            routing_loader:
                default_format: json
            param_fetcher_listener: true
            body_listener: true
            #disable_csrf_role: ROLE_USER
            body_converter:
                enabled: true
            view:
                view_response_listener: force
        
        nelmio_cors:
            defaults:
                allow_credentials: false
                allow_origin: []
                allow_headers: []
                allow_methods: []
                expose_headers: []
                max_age: 0
            paths:
                '^/api/':
                    allow_origin: ['*']
                    allow_headers: ['*']
                    allow_methods: ['POST', 'PUT', 'GET', 'DELETE']
                    max_age: 3600
        
        sensio_framework_extra:
            request: { converters: true }
            view:    { annotations: false }
            router:  { annotations: true }


en AppKernel.php:

        new Voryx\RESTGeneratorBundle\VoryxRESTGeneratorBundle(),
        new FOS\RestBundle\FOSRestBundle(),
        new JMS\SerializerBundle\JMSSerializerBundle($this),
        new Nelmio\CorsBundle\NelmioCorsBundle(),
        new OAuth2\ServerBundle\OAuth2ServerBundle(),

app/console doc:d:c
app/console doc:gen:entity
app/console d:s:c

app/console generate:doctrine:form AppBundle:Montana
app/console voryx:generate:rest

app/console OAuth2:CreateScope publica "Acceso sin necesidad de autentificar al usuario"
app/console OAuth2:CreateClient montanas 'http://localhost:3000/token' client_credentials publica


Las acciones se protegen con 

        $oauthServer = $this->get('oauth2.server');

        if (!$oauthServer->verifyResourceRequest(\OAuth2\Request::createFromGlobals())) {
            return FOSView::create('UnAuthorized', Codes::HTTP_FORBIDDEN);
        }

Ver la función ``cgetAction`` del controlador de ejemplo.

Obtenemos token:

POST http://localhost:8000/token

headers
Content-Type: application/x-www-form-urlencoded

body
grant_type=client_credentials&scope=publica&client_id=montanas&client_secret=116fqai8mv808w8oswkoos8gg0wskcc



