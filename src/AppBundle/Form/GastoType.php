<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GastoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('motivo')
            ->add('cantidad')
            ->add('fecha_desde', 'datetime', array('widget' => 'single_text'))
            ->add('fecha_hasta', 'datetime', array('widget' => 'single_text'))
            ->add('grupo', 'voryx_entity', array(
                    'class' => 'AppBundle\Entity\Grupo'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Gasto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_gasto';
    }
}
