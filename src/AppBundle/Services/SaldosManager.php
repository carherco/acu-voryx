<?php

namespace AppBundle\Services;

use Intefnautas\ACUBundle\Entity\Gasto;
use Intefnautas\ACUBundle\Entity\Aportacion;

/**
 * Ajustador de Cuentas
 * servicio: SaldosManager
 * descripción: Servicio encargado de toda la lógica de negocio (o modelo)
 *              referente al calculo de los Saldos. No tiene entidad en base de 
 *              datos.
 * @author: Carlos Herrera Conejero
 * @version git: 06-03-2014
 * notas: Este servicio será usado por el controlador SaldosController, una vez
 *        haya leído la REQUEST y sepa que tipo de petición se ha hecho.
 */
class SaldosManager {

    /**
     *
     * @var type 
     */
    protected $em, $connection, $val_service;

    /**
     * 
     * @param type $em
     * @param type $val_service
     */
    public function __construct($em, $dbalConnection) {
        $this->em = $em;
        $this->connection = $dbalConnection;
    }

    /**
     * Lectura de la colección de todas las saldos. Devuelve las saldos en un
     * array o un mensaje de error en caso de que lo hubiera
     * 
     * @param type $id_grupo
     * @return type
     */
    public function readSaldoCollection($id_grupo) {
        try {
            //Usuarios del grupo
            $grupo = $this->em->getRepository('AppBundle:Grupo')->find($id_grupo);
            $miembros_grupo = $grupo->getMiembros();
            
            //---------Lectura de la colección de gastos ---------//
            $sql = "SELECT pg.participante_id, SUM(pg.cantidad) as cantidad "
                    . "FROM participaciones_gastos pg INNER JOIN gasto g ON g.id = pg.gasto_id "
                    . "WHERE g.grupo_id = ? "
                    . "GROUP BY pg.participante_id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(1, $id_grupo);
            $stmt->execute();
            $gastos = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            //---------Lectura de la colección de aportaciones (fianzas)---------//
            $sql = "SELECT aportador_id, SUM(cantidad) as cantidad "
                    . "FROM aportacion "
                    . "WHERE grupo_id = ? "
                    . "AND receptor_id = 1 "
                    . "GROUP BY aportador_id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(1, $id_grupo);
            $stmt->execute();
            $aportaciones1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            //---------Lectura de la colección de aportaciones (Fondo común)---------//
            $sql = "SELECT aportador_id, SUM(cantidad) as cantidad "
                    . "FROM aportacion "
                    . "WHERE grupo_id = ? "
                    . "AND receptor_id = 2 "
                    . "GROUP BY aportador_id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(1, $id_grupo);
            $stmt->execute();
            $aportaciones2 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
             //---------Lectura de la colección de aportaciones (Fondo común)---------//
            $sql = "SELECT receptor_id, SUM(cantidad) as cantidad "
                    . "FROM aportacion "
                    . "WHERE grupo_id = ? "
                    . "AND aportador_id = 3 "
                    . "GROUP BY receptor_id";
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(1, $id_grupo);
            $stmt->execute();
            $aportaciones3 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            
            //Merge de los datos
            $listaUsuarios = array();
            foreach ($miembros_grupo as $miembro_grupo) {
                if(!$miembro_grupo->esEspecial()) {
                    $id_usuario = $miembro_grupo->getUsuario()->getId();
                    $listaUsuarios[$id_usuario]['id'] = $id_usuario;
                    $listaUsuarios[$id_usuario]['nombre'] = $miembro_grupo->getUsuario()->getNombre();
                    $listaUsuarios[$id_usuario]['fecha_entrada'] = $miembro_grupo->getFecha_entrada();
                    $listaUsuarios[$id_usuario]['fecha_salida'] = $miembro_grupo->getFecha_salida();
                    $listaUsuarios[$id_usuario]['activo'] = $miembro_grupo->getActivo();
                }
            }
            
            foreach ($gastos as $gasto) {
                $listaUsuarios[$gasto['participante_id']]['gastos'] = $gasto['cantidad'];
            }
            foreach ($aportaciones1 as $aportacion) {
                $listaUsuarios[$aportacion['aportador_id']]['fianzas'] = $aportacion['cantidad'];
            }
            foreach ($aportaciones2 as $aportacion) {
                $listaUsuarios[$aportacion['aportador_id']]['aportaciones'] = $aportacion['cantidad'];
            }
            foreach ($aportaciones3 as $aportacion) {
                $listaUsuarios[$aportacion['receptor_id']]['ajustes'] = $aportacion['cantidad'];
            }
    
            $listaSaldos = array();
            foreach ($listaUsuarios as $id => $usuario) {
                $saldo = array();
                $saldo['idusuario'] = $id;
                $saldo['nombre'] = $usuario['nombre'];    
                $saldo['fecha_entrada'] = $usuario['fecha_entrada'];    
                $saldo['fecha_salida'] = $usuario['fecha_salida']; 
                $saldo['activo'] = $usuario['activo']; 
                $saldo['gastos'] = isset($usuario['gastos'])?(float)$usuario['gastos']:0;
                $saldo['fianzas'] = isset($usuario['fianzas'])?(float)$usuario['fianzas']:0;
                $saldo['aportaciones'] = isset($usuario['aportaciones'])?(float)$usuario['aportaciones']:0;
                $saldo['ajustes'] = isset($usuario['ajustes'])?(float)$usuario['ajustes']:0;
                $saldo['saldototal'] = $saldo['fianzas']+$saldo['aportaciones']+$saldo['ajustes']-$saldo['gastos'];
                $saldo['saldosinfianza'] = $saldo['aportaciones']+$saldo['ajustes']-$saldo['gastos'];
                $listaSaldos[] = $saldo;
            }

            //----------------------------------------------------------------//
            $resultado['data'] = $listaSaldos;
            $resultado['statusCode'] = 200;
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }


    /**
     * Serializa un saldo en un array
     * 
     * @param type $saldo
     * @return type
     */
    private function serializarSaldo($saldo) {
        $datosSaldo['id'] = $saldo->getId();
        $datosSaldo['acreedor']['id'] = $saldo->getAcreedor()->getId();
        $datosSaldo['acreedor']['nombre'] = $saldo->getAcreedor()->__toString();
        $datosSaldo['deudor']['id'] = $saldo->getDeudor()->getId();
        $datosSaldo['deudor']['nombre'] = $saldo->getDeudor()->__toString();
        $datosSaldo['cantidad'] = $saldo->getCantidad();
        return $datosSaldo;
    }

}
