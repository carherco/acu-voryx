<?php

namespace AppBundle\Services;

use JMS\TranslationBundle\Annotation\Ignore;

/**
 * Ajustador de Cuentas
 * servicio: ValidacionService
 * descripción: Servicio que engloba las funcionalidades de seguridad y
 *              validación necesarias para ciertas acciones.
 * @author: Álvaro Peláez Santana
 * @version git: 20-09-2013
 */
class ValidacionService {

    protected $em, $validator, $translator;

    /**
     * 
     * @param type $em
     * @param type $validator
     * @param type $translator
     */
    public function __construct($em, $validator, $translator) {
        $this->em = $em;
        $this->validator = $validator;
        $this->translator = $translator;
    }

    /**
     * 
     * @param type $id_grupo
     * @param type $usuario
     * @return type
     */
    public function validarUsuarioGrupo($id_grupo, $usuario) {
        $grupo = $this->em->getRepository('AppBundle:Grupo')->find($id_grupo);
        if ($grupo) {
            if (!$grupo->esMiembro($usuario) && ($usuario->getId() !== $grupo->getAdministrador()->getId())) {
                $resultado['statusCode'] = 403;
                $resultado['data'] = $this->translator->trans('error.permisos.grupo');
                return $resultado;
            }
        } else {
            $resultado['statusCode'] = 404;
            $resultado['data'] = $this->translator->trans('error.noencontrado.grupo');
            return $resultado;
        }
    }

    /**
     * 
     * @param type $pago
     * @param type $usuario
     * @param type $grupo
     * @return type
     */
    public function validarAdminPago($pago, $usuario, $grupo) {
        if (($pago->getPagador()->getId() !== $usuario->getId()) && ($grupo->getAdministrador()->getId() !== $usuario->getId())) {
            $resultado['statusCode'] = 403;
            $resultado['data'] = $this->translator->trans('error.permisos.pago');
            return $resultado;
        }
    }
    
    /**
     * 
     * @param type $pago
     * @param type $usuario
     * @param type $grupo
     * @return type
     */
    public function validarAdminGasto($gasto, $usuario, $grupo) {
//        if (($pago->getPagador()->getId() !== $usuario->getId()) && ($grupo->getAdministrador()->getId() !== $usuario->getId())) {
//            $resultado['statusCode'] = 403;
//            $resultado['data'] = $this->translator->trans('error.permisos.pago');
//            return $resultado;
//        }
    }
    
    /**
     * 
     * @param type $pago
     * @param type $usuario
     * @param type $grupo
     * @return type
     */
    public function validarAdminAportacion($aportacion, $usuario, $grupo) {
//        if (($pago->getPagador()->getId() !== $usuario->getId()) && ($grupo->getAdministrador()->getId() !== $usuario->getId())) {
//            $resultado['statusCode'] = 403;
//            $resultado['data'] = $this->translator->trans('error.permisos.pago');
//            return $resultado;
//        }
    }

    /**
     * 
     * @param type $deuda
     * @param type $usuario
     * @param type $grupo
     * @return type
     */
    public function validarAdminDeuda($deuda, $usuario, $grupo) {
        if (($deuda->getAcreedor()->getId() !== $usuario->getId()) && ($grupo->getAdministrador()->getId() !== $usuario->getId())) {
            $resultado['statusCode'] = 403;
            $resultado['data'] = $this->translator->trans('error.permisos.deuda');
            return $resultado;
        }
    }

    /**
     * 
     * @param type $id
     * @param type $usuario
     * @return type
     */
    public function validarUsuario($id, $usuario) {
        if ($id != $usuario->getId()) {
            $resultado['statusCode'] = 403;
            $resultado['data'] = $this->translator->trans('error.permisos.usuario');
            return $resultado;
        }
    }

    /**
     * 
     * @param type $entidad
     * @return boolean
     */
    public function validarEntidad($entidad) {
        $errors = $this->validator->validate($entidad);
        if (count($errors) > 0) {
            $resultado['statusCode'] = 422;
            foreach ($errors as $error) {
                $resultado['data'][$error->getPropertyPath()] = $this->translator->trans(/** @Ignore */ $error->getMessage(), $error->getMessageParameters(), 'validators');
            }
            return $resultado;
        } else {
            return false;
        }
    }

}
