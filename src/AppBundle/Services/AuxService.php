<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Response;

/**
 * Ajustador de Cuentas
 * servicio: AuxService
 * descripción: Servicio que engloba un conjunto de funciones auxiliares, usadas
 *              en distintas partes del proyecto ACU.
 * @author: Álvaro Peláez Santana
 * @version git: 20-09-2013
 */
class AuxService {

    /**
     * 
     * @param type $data
     * @param type $status_code
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respuestaJSON($data, $status_code) {
        $json = json_encode($data);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json;charset=UTF-8');
        $response->setContent($json);
        $response->setStatusCode($status_code);
        return $response;
    }

}
