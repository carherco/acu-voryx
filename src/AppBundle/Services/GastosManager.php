<?php

namespace AppBundle\Services;

use AppBundle\Entity\Gasto;
use AppBundle\Entity\ParticipacionGasto;

/**
 * Ajustador de Cuentas
 * servicio: GastosManager
 * descripción: Servicio encargado de toda la lógica de negocio (o modelo)
 *              referente a la entidad Gastos. Contiene la funcionalidad para la
 *              creación, edición, lectura y borrado de los gastos, persistiendo
 *              todos estos cambios en la base de datos, y actualizando las
 *              deudas entre usuarios según estos cambios.
 * @author: Álvaro Peláez Santana
 * @version git: 20-09-2013
 * notas: Este servicio será usado por el controlador GastosController, una vez
 *        haya leído la REQUEST y sepa que tipo de petición se ha hecho.
 */
class GastosManager {

    /**
     *
     * @var type 
     */
    protected $em, $connection, $val_service, $notifier;

    /**
     * 
     * @param type $em
     * @param type $val_service
     */
    public function __construct($em, $dbalConnection, $val_service, $notifier) {
        $this->em = $em;
        $this->connection = $dbalConnection;
        $this->val_service = $val_service;
        $this->notifier = $notifier;
    }

    /**
     * Creación de un nuevo gasto, los datos del gasto vienen como parámetro de
     * entrada mediante un JSON. Devuelve un mensaje de exito, o de error en
     * caso de que lo hubiera
     * 
     * @param type $json
     * @param type $id_grupo
     * @param type $usuario
     * @return type
     */
    public function createGasto($json, $id_grupo, $usuario) {
        error_reporting(0);
        try {
            //----------------Creación de un nuevo gasto-----------------------//
            $grupo = $this->em->getRepository('AppBundle:Grupo')->find($id_grupo);
            $gasto = $this->deserializarGasto($json, $usuario, $grupo);
            //---------------------Validación---------------------------------//
            if (($resultado = $this->val_service->validarEntidad($gasto))) {
                return $resultado;
            }
            //---------------------Persistir----------------------------------//            
            $this->em->persist($gasto);
            $this->em->flush();
            $this->actualizarParticipacionesGasto($gasto);
            
            $this->notifier->notificarGastoCreate($gasto);
//            $this->actualizarDeudasParticipantes($gasto);
//            $this->dm->optimizarDeudas($grupo);
            //-----------------Devolver resultado-----------------------------//
            $resultado['data'] = "Gasto creado correctamente";
            $resultado['statusCode'] = 200;
            //-------------------Manejo de excepciones------------------------//
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Doctrine\ORM\OptimisticLockException $flushexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $flushexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }

    /**
     * Lectura del gasto cuya id viene como parámetro de entrada. Devuelve el
     * gasto en un array o un mensaje de error en caso de que lo hubiera.
     * 
     * @param type $id
     * @return type
     */
    public function readGasto($id) {
        try {
            //--------------Lectura del gasto----------------------------------//
            $gasto = $this->em->getRepository('AppBundle:Gasto')->find($id);
            //------------------Devolver resultado----------------------------//
            $resultado['data'] = $this->serializarGasto($gasto);
            $resultado['statusCode'] = 200;
            //-------------------Manejo de excepciones------------------------//
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }

    /**
     * Lectura de la colección de todos los gastos. Devuelve los gastos en un
     * array o un mensaje de error en caso de que lo hubiera.
     * 
     * @param type $id_grupo
     * @return type
     */
    public function readGastoCollection($id_grupo) {
        try {
            //--------Lectura de la colección y guardado en un array----------//
            $gastos = $this->em->getRepository('AppBundle:Gasto')->findByGrupo($id_grupo,array('id'=>'DESC'));
            $listaGastos = array();
            foreach ($gastos as $gasto) {
                $listaGastos[] = $this->serializarGasto($gasto);
            }
            //------------------Devolver resultado----------------------------//    
            $resultado['data'] = $listaGastos;
            $resultado['statusCode'] = 200;
            //---------------Manejo de excepciones----------------------------// 
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }

    /**
     * Actualización del gasto cuya id viene como parámetro de entrada, asi como
     * los nuevos valores del gasto. Devuelve un mensaje de exito o de error en
     * caso de que lo hubiera.
     * 
     * @param type $id
     * @param type $json
     * @param type $usuario
     * @return type
     */
    public function updateGasto($id, $json, $usuario) {
        error_reporting(0);
        try {
            //----------Actualización de un gasto existente--------------------//
            $gasto = $this->em->getRepository('AppBundle:Gasto')->find($id);
            $grupo = $gasto->getGrupo();
            if (($resultado = $this->val_service->validarAdminGasto($gasto, $usuario, $grupo))) {
                return $resultado;
            }
            $this->actualizarDeudasParticipantes($gasto, true);
            $this->deserializarGasto($json, $usuario, $grupo, $gasto);
            //-------------------------Validación-----------------------------//
            if (($resultado = $this->val_service->validarEntidad($gasto))) {
                return $resultado;
            }
            //--------------------------Persistir-----------------------------//
            $this->em->flush();
            $this->actualizarDeudasParticipantes($gasto);
            $this->dm->optimizarDeudas($grupo);
            $this->notifier->notificarGastoUpdate($gasto);
            //------------------Devolver resultado----------------------------//
            $resultado['data'] = "Gasto $id actualizado";
            $resultado['statusCode'] = 200;
            //---------------Manejo de excepciones----------------------------//
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Doctrine\ORM\OptimisticLockException $flushexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $flushexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }

    /**
     * Eliminación del gasto cuya id viene como parámetro de entrada. Devuelve un
     * mensaje de exito o de error en caso de que lo hubiera.
     * 
     * @param type $id
     * @param type $usuario
     * @return type
     */
    public function deleteGasto($id, $usuario) {
        try {
            //--------------------Eliminación del gasto------------------------//
            $gasto = $this->em->getRepository('AppBundle:Gasto')->find($id);
            $grupo = $gasto->getGrupo();
//            if (($resultado = $this->val_service->validarAdminGasto($gasto, $usuario, $grupo))) {
//                return $resultado;
//            }
            //---------------------Actualizar deudas--------------------------//
//            $this->actualizarDeudasParticipantes($gasto, true);
            //--------------------------Persistir-----------------------------//
            $this->em->remove($gasto);
            $this->em->flush();
            $this->dm->optimizarDeudas($grupo);
            $this->notifier->notificarGastoDelete($gasto);
            //------------------Devolver resultado----------------------------//
            $resultado['data'] = "Gasto $id borrado";
            $resultado['statusCode'] = 200;
            //---------------Manejo de excepciones----------------------------//
        } catch (\ErrorException $mapexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $mapexc->getMessage();
        } catch (\Doctrine\ORM\OptimisticLockException $flushexc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $flushexc->getMessage();
        } catch (\Exception $exc) {
            $resultado['statusCode'] = 500;
            $resultado['data'] = $exc->getMessage();
        }
        return $resultado;
    }

    /**
     * Serializa un gasto en un array
     * 
     * @param \AppBundle\Entity\Gasto $gasto
     * @return type
     */
    private function serializarGasto($gasto) {
        $datosGasto['id'] = (int)$gasto->getId();
        $datosGasto['cantidad'] = $gasto->getCantidad();
        $datosGasto['motivo'] = $gasto->getMotivo();
        $datosGasto['fechadesde'] = $gasto->getFechaDesde()->format('d-m-Y');
        $datosGasto['fechahasta'] = $gasto->getFechaHasta()->format('d-m-Y');
        $participantes_gasto = $gasto->getParticipantesGasto();
        $datosGasto['participantes'] = array();
        foreach ($participantes_gasto as $j => $participante_gasto) {
            $datosGasto['participantes'][$j]['id'] = (int)$participante_gasto->getParticipante()->getId();
            $datosGasto['participantes'][$j]['nombre'] = $participante_gasto->getParticipante()->getNombre();
            $datosGasto['participantes'][$j]['peso'] = (int)$participante_gasto->getPeso();
            $datosGasto['participantes'][$j]['cantidad'] = $participante_gasto->getCantidad();
        }
        return $datosGasto;
    }

    /**
     * Deserializa un gasto de un json a un objeto
     * 
     * @param type $json
     * @param type $usuario
     * @param type $grupo
     * @param \AppBundle\Entity\Gasto $gasto
     * @return \AppBundle\Entity\Gasto
     */
    private function deserializarGasto($json, $usuario, $grupo, $gasto = null) {
        $datosGasto = json_decode($json, true);
        if (!$gasto) {
            $gasto = new Gasto();
            $gasto->setGrupo($grupo);
        } else {
            foreach ($gasto->getParticipantes() as $participante) {
                $gasto->removeParticipante($participante);
            }
        }
        $gasto->setCantidad($datosGasto['cantidad']);
        $gasto->setMotivo($datosGasto['motivo']);       
        $gasto->setFechaDesde(\DateTime::createFromFormat('Y-m-d', $datosGasto['fechadesde']));
        $gasto->setFechaHasta(\DateTime::createFromFormat('Y-m-d', $datosGasto['fechahasta']));
   
        foreach ($datosGasto['participantes'] as $participante_data) {
            $participante = $this->em->getRepository('AppBundle:Usuario')->find($participante_data['id']);
            $participacionGasto = new ParticipacionGasto();
            $participacionGasto->setParticipante($participante);
            $participacionGasto->setPeso(1);
            $participacionGasto->setCantidad(0);
            $participacionGasto->setGasto($gasto);
            $gasto->addParticipacionGasto($participacionGasto);
        }
        return $gasto;
    }
    
    /**
     * Función que actualiza las participaciones de un gasto.
     * 
     * @todo No se están incluyendo los pesos de los participantes
     * 
     * @param Gasto $gasto
     */
    public function actualizarParticipacionesGasto(Gasto $gasto) {
        $grupo = $gasto->getGrupo();
        $participantesGasto = $gasto->getParticipantesGasto();
        $numParticipantes = count($participantesGasto);
        $interval = $gasto->getFechaDesde()->diff($gasto->getFechaHasta());
        $numDiasGasto = $interval->days+1;
        $gasto_dia = $gasto->getCantidad() / $numDiasGasto;
  
        $participantesFactura = array();
        foreach ($participantesGasto as $participante) {
            $id_participante = $participante->getParticipante()->getId();
            $participantesFactura[$id_participante]['id'] = $id_participante;
            $participantesFactura[$id_participante]['peso'] = 1;
            $participantesFactura[$id_participante]['cantidad'] = 0;
        }
        
        $participantesIniciales = $this->calcularParticipantesIniciales($gasto);
        $entradas = $this->calcularEntradasParticipantes($gasto);
        $salidas = $this->calcularSalidasParticipantes($gasto);
               
        //Empieza el algoritmo
        $participantesDia = $participantesIniciales;
        $gasto_dia_participante = $gasto_dia / count($participantesDia);
        for($d = 1; $d<=$numDiasGasto; $d++) {
            //Si alguien entra en el grupo este día, se le incluye
            if(isset($entradas[$d])){
                $participantesDia = array_merge($participantesDia,$entradas[$d]);
                $gasto_dia_participante = $gasto_dia / count($participantesDia);
            }
        
            //Se le añade a cada participante el gasto de este día
            foreach($participantesDia as $id_participante){
                $participantesFactura[$id_participante]['cantidad'] += $gasto_dia_participante;
            }
            
            //Si alguien sale del grupo este día, se le excluye
            if(isset($salidas[$d])){
                $participantesDia = array_diff($participantesDia,$salidas[$d]);
                $gasto_dia_participante = $gasto_dia / count($participantesDia);
            }
        }
   
        //Actualizando las cantidades de los participantes
        foreach ($participantesGasto as $participante) {
            $id_participante = $participante->getParticipante()->getId();
            $participante->setCantidad($participantesFactura[$id_participante]['cantidad']);
            $this->em->persist($participante);
        }

        $this->em->flush();
    }
    
    /**
     * Calcula los tramos de un gasto
     * 
     * @param Gasto $gasto
     */
    private function calcularParticipantesIniciales(Gasto $gasto) {
        $grupo = $gasto->getGrupo();
        
        //---------Participantes iniciales en el gasto ---------//
        $sql = "SELECT participante_id, peso 
                FROM `miembros_grupos` mg
                INNER JOIN participaciones_gastos pg ON mg.usuario_id = pg.participante_id
                WHERE mg.grupo_id = ?
                  AND pg.gasto_id = ?
                  AND `fecha_entrada` <= ? 
                  AND (`fecha_salida` IS NULL OR  `fecha_salida` >= ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(1, $grupo->getId());
        $stmt->bindValue(2, $gasto->getId());
        $stmt->bindValue(3, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->bindValue(4, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->execute();
        $participantes = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        $salida = array();
        foreach ($participantes as $participante){
            $salida[] = (int)$participante['participante_id'];
        }
        return $salida;
        
    }
    
    /**
     * Calcula los tramos de un gasto
     * 
     * @param Gasto $gasto
     */
    private function calcularEntradasParticipantes(Gasto $gasto) {
        $grupo = $gasto->getGrupo();
        
        //---------Participantes iniciales en el gasto ---------//
        $sql = "SELECT participante_id, peso, DATEDIFF(`fecha_entrada`,?)+1 as diaentrada
                FROM `miembros_grupos` mg
                INNER JOIN participaciones_gastos pg ON mg.usuario_id = pg.participante_id
                WHERE mg.grupo_id = ?
                  AND pg.gasto_id = ?
                  AND (`fecha_entrada` > ? AND `fecha_entrada` <= ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(1, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->bindValue(2, $grupo->getId());
        $stmt->bindValue(3, $gasto->getId());
        $stmt->bindValue(4, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->bindValue(5, $gasto->getFechaHasta()->format('Y-m-d'));
        $stmt->execute();
        $participantes = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        $salida = array();
        foreach ($participantes as $participante){
            $salida[$participante['diaentrada']][] = (int)$participante['participante_id'];
        }
        return $salida;
        
    }
    
    /**
     * Calcula los tramos de un gasto
     * 
     * @param Gasto $gasto
     */
    private function calcularSalidasParticipantes(Gasto $gasto) {
        $grupo = $gasto->getGrupo();
        
        //---------Participantes iniciales en el gasto ---------//
        $sql = "SELECT participante_id, peso, DATEDIFF(fecha_salida,?)+1 as diasalida
                FROM `miembros_grupos` mg
                INNER JOIN participaciones_gastos pg ON mg.usuario_id = pg.participante_id
                WHERE mg.grupo_id = ?
                  AND pg.gasto_id = ?
                  AND `fecha_salida` IS NOT NULL
                  AND (`fecha_salida` >= ? AND `fecha_salida` < ?)";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(1, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->bindValue(2, $grupo->getId());
        $stmt->bindValue(3, $gasto->getId());
        $stmt->bindValue(4, $gasto->getFechaDesde()->format('Y-m-d'));
        $stmt->bindValue(5, $gasto->getFechaHasta()->format('Y-m-d'));
        $stmt->execute();
        $participantes = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        $salida = array();
        foreach ($participantes as $participante){
            $salida[$participante['diasalida']][] = (int)$participante['participante_id'];
        }
        return $salida;

    }

    /**
     * Función que actualiza las deudas de todos los participantes en un gasto.
     * En caso de que quitar sea verdadero la deuda se crea a la inversa (el
     * pagador deberá a los deudores). Esto último es útil para eliminar gastos.
     * 
     * @param type $gasto
     * @param type $quitar
     */
    private function actualizarDeudasParticipantes($gasto, $quitar = null) {
        $participantes = $gasto->getParticipantesGasto();
        $pagador = $gasto->getPagador();
        $grupo = $gasto->getGrupo();
        $deuda_individual = $gasto->getCantidad() / $participantes->count();
        foreach ($participantes as $participante) {
            if ($pagador->getId() != $participante->getId()) {
                if ($quitar) {
                    $this->dm->actualizarDeuda($participante, $pagador, $grupo, $deuda_individual);
                } else {
                    $this->dm->actualizarDeuda($pagador, $participante, $grupo, $deuda_individual);
                }
            }
        }
        $this->em->flush();
    }

}
