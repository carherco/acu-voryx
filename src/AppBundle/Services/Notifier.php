<?php

namespace AppBundle\Services;

/**
 * Description of Notificador
 *
 * @author carlos
 */
class Notifier {

    private $mailer;
    private $em;
    private $templating;

    public function __construct($mailer, $em, $templating) {
        $this->mailer = $mailer;
        $this->em = $em;
        $this->templating = $templating;
    }


    /**
     * 
     * @param type $destino String donde viene el email del destinatario
     * @param type $asunto String con el asunto del mensaje
     * @param type $cuerpo String con el cuerpo del mensaje
     */
    public function enviarEmail($destino, $asunto, $cuerpo) {

        $from_email = 'notificaciones.acu@carherco.es';
        $from_name = 'Ajustador de cuentas';

        $message = \Swift_Message::newInstance()
                ->setContentType('text/html')
                ->setSubject($asunto)
                ->setFrom($from_email, $from_name)
                ->setTo($destino)
                ->setBody($cuerpo)
        ;

        return $this->mailer->send($message);
    }

    public function notificarPago(Entity\Pago $pago)
    {
        $asunto = "Nuevo pago en el grupo";
        $cuerpo = "¡Alguien ha hecho un pago en el grupo!";

        $users = $pago->getParticipantes();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarAportacionCreate(\AppBundle\Entity\Aportacion $aportacion)
    {
        $asunto = "Nueva aportación en el grupo";
        $plantilla = 'IntefACUBundle:Mail:aportacion_new.html.twig';
        $cuerpo = $this->templating->render($plantilla,array('aportacion'=>$aportacion));

        $users = $aportacion->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarAportacionUpdate(\AppBundle\Entity\Aportacion $aportacion)
    {
        $asunto = "Aportación modificada";
        $plantilla = 'IntefACUBundle:Mail:aportacion_update.html.twig';
        $cuerpo = $this->templating->render($plantilla,array('aportacion'=>$aportacion));

        $users = $aportacion->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarAportacionDelete(\AppBundle\Entity\Aportacion $aportacion)
    {
        $asunto = "Aportación eliminada";
        $plantilla = 'IntefACUBundle:Mail:aportacion_delete.html.twig';
        $cuerpo = $this->templating->render($plantilla,array('aportacion'=>$aportacion));

        $users = $aportacion->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarGastoCreate(\AppBundle\Entity\Gasto $gasto)
    {
        $asunto = "Nuevo gasto en el grupo";
        $cuerpo = "¡Alguien ha añadido un gasto al grupo!";

        $users = $gasto->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarGastoUpdate(\AppBundle\Entity\Gasto $gasto)
    {
        $asunto = "Gasto modificado";
        $cuerpo = "¡Alguien ha modificado un gasto del grupo!";

        $users = $gasto->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
    public function notificarGastoDelete(\AppBundle\Entity\Gasto $gasto)
    {
        $asunto = "Gasto eliminado";
        $cuerpo = "¡Alguien ha eliminado un gasto en el grupo!";

        $users = $gasto->getGrupo()->getAllUsuarios();

        foreach($users as $user)
        {
            $this->enviarEmail($user->getEmail(), $asunto, $cuerpo);
        }
        
    }
    
}