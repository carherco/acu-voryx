<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Deuda
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\DeudaRepository")
 */
class Deuda {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="deudas_acreedor") 
     * @Assert\NotBlank(message="deuda.acreedor.novacio")
     * @Assert\Type(type="object", message="deuda.acreedor.tipoinvalido")
     */
    private $acreedor;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="deudas_deudor")     
     * @Assert\NotBlank(message="deuda.deudor.novacio")
     * @Assert\Type(type="object", message="deuda.deudor.tipoinvalido")
     */
    private $deudor;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\NotBlank(message="deuda.cantidad.novacio")
     * @Assert\Type(type="numeric", message="deuda.cantidad.tipoinvalido")
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="deudas")
     */
    private $grupo;

    function __construct($acreedor = null, $deudor= null, $cantidad= null, $grupo= null) {
        $this->cantidad = $cantidad;
        $this->acreedor = $acreedor;
        $this->deudor = $deudor;
        $this->grupo = $grupo;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return Deuda
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    /**
     * Set acreedor
     *
     * @param \AppBundle\Entity\Usuario $acreedor
     * @return Deuda
     */
    public function setAcreedor(\AppBundle\Entity\Usuario $acreedor = null) {
        $this->acreedor = $acreedor;

        return $this;
    }

    /**
     * Get acreedor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getAcreedor() {
        return $this->acreedor;
    }

    /**
     * Set deudor
     *
     * @param \AppBundle\Entity\Usuario $deudor
     * @return Deuda
     */
    public function setDeudor(\AppBundle\Entity\Usuario $deudor = null) {
        $this->deudor = $deudor;

        return $this;
    }

    /**
     * Get deudor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getDeudor() {
        return $this->deudor;
    }

    /**
     * Set grupo
     *
     * @param \AppBundle\Entity\Grupo $grupo
     * @return Deuda
     */
    public function setGrupo(\AppBundle\Entity\Grupo $grupo = null) {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \AppBundle\Entity\Grupo 
     */
    public function getGrupo() {
        return $this->grupo;
    }

}
