<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Pago
 * 
 * @JMS\ExclusionPolicy("all")
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GastoRepository")
 */
class Gasto {

    /**
     * @var integer
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="facturas") 
     */
    private $grupo;

    /**
     * @var string
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="motivo", type="string", length=255)
     * @Assert\Type(type="string", message="factura.motivo.tipoinvalido")
     */
    private $motivo;
    
    /**
     * @var float
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\NotBlank(message="factura.cantidad.novacio")
     * @Assert\Type(type="numeric", message="factura.cantidad.tipoinvalido")
     */
    private $cantidad;

    /**
     * @var \DateTime
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="fecha_desde", type="datetime")
     */
    private $fecha_desde;
    
    /**
     * @var \DateTime
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="fecha_hasta", type="datetime")
     */
    private $fecha_hasta;

    /**
     * @JMS\Expose
     * @JMS\MaxDepth(3)
     * 
     * @ORM\OneToMany(targetEntity="ParticipacionGasto", mappedBy="gasto", cascade={"persist", "remove"}) 
     */
    private $participaciones_gasto;
    
    
    private $participantes;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return Gasto
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }
    
    /**
     * Set motivo
     *
     * @param string $motivo
     * @return Gasto
     */
    public function setMotivo($motivo) {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo
     *
     * @return string 
     */
    public function getMotivo() {
        return $this->motivo;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->participaciones_gasto = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add participaciones_gasto
     *
     * @param \AppBundle\Entity\ParticipacionGasto $participacion
     * @return Gasto
     */
    public function addParticipacionGasto(\AppBundle\Entity\ParticipacionGasto $participacion) {
        $this->participaciones_gasto[] = $participacion;

        return $this;
    }

    /**
     * Remove participaciones_gasto
     *
     * @param \AppBundle\Entity\ParticipacionGasto $participacion
     */
    public function removeParticipanteGasto(\AppBundle\Entity\ParticipacionGasto $participacion) {
        $this->participaciones_gasto->removeElement($participacion);
    }

    /**
     * Get participaciones_gasto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipantesGasto() {
        return $this->participaciones_gasto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Gasto
     */
    public function setFechaDesde($fecha) {
        $this->fecha_desde = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFechaDesde() {
        return $this->fecha_desde;
    }
    
    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Gasto
     */
    public function setFechaHasta($fecha) {
        $this->fecha_hasta = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFechaHasta() {
        return $this->fecha_hasta;
    }
          


    /**
     * Set grupo
     *
     * @param \AppBundle\Entity\Grupo $grupo
     * @return Gasto
     */
    public function setGrupo(\AppBundle\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;
    
        return $this;
    }

    /**
     * Get grupo
     *
     * @return \AppBundle\Entity\Grupo 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }
    
    public function getPagador()
    {
        return "Método getPagador no definido";
    }
}