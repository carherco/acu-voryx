<?php

namespace AppBundle\Entity;

use OAuth2\ServerBundle\Entity\User as BaseUser;
//use FOS\UserBundle\Model\User as BaseUser;
//use Symfony\Component\Security\Core\User\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Usuario
 * 
 * @JMS\ExclusionPolicy("all")
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UsuarioRepository")
 */
//class Usuario extends BaseUser {
class Usuario {

    /**
     * @var integer
     * 
     * @JMS\Expose
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     * @Assert\Type(type="string", message="usuario.nombre.tipoinvalido")
     */
    private $nombre;

    /**
     * @var string
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     * @Assert\Type(type="string", message="usuario.apellidos.tipoinvalido")
     */
    private $apellidos;

    /**
     * @ORM\OneToMany(targetEntity="Pago", mappedBy="pagador") 
     */
    private $pagos;
    
    /**
     * @ORM\OneToMany(targetEntity="Aportacion", mappedBy="aportador") 
     */
    private $aportaciones;

    /**
     * @ORM\ManyToMany(targetEntity="Pago", mappedBy="participantes")
     */
    private $participaciones;

    /**
     * @ORM\OneToMany(targetEntity="Grupo", mappedBy="administrador") 
     */
    private $grupos_administrador;

    /**
     * @JMS\Expose
     * 
     * @ORM\OneToMany(targetEntity="MiembroGrupo", mappedBy="usuario")
     */
    private $grupos_miembro;

    /**
     * @ORM\OneToMany(targetEntity="Deuda", mappedBy="acreedor") 
     */
    private $deudas_acreedor;

    /**
     * @ORM\OneToMany(targetEntity="Deuda", mappedBy="deudor") 
     */
    private $deudas_deudor;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Usuario
     */
    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos() {
        return $this->apellidos;
    }

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->pagos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->aportaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deudas_acreedor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deudas_deudor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return "$this->nombre $this->apellidos";
    }

    /**
     * Add pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     * @return Usuario
     */
    public function addPago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     */
    public function removePago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }
    
    /**
     * Add aportaciones
     *
     * @param \AppBundle\Entity\Aportacion $aportaciones
     * @return Usuario
     */
    public function addAportacion(\AppBundle\Entity\Aportacion $aportaciones) {
        $this->aportaciones[] = $aportaciones;

        return $this;
    }

    /**
     * Remove aportaciones
     *
     * @param \AppBundle\Entity\Aportacion $aportaciones
     */
    public function removeAportacion(\AppBundle\Entity\Aportacion $aportaciones) {
        $this->aportaciones->removeElement($aportaciones);
    }

    /**
     * Get aportaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAportaciones() {
        return $this->aportaciones;
    }

    /**
     * Add deudas_acreedor
     *
     * @param \AppBundle\Entity\Deuda $deudasAcreedor
     * @return Usuario
     */
    public function addDeudasAcreedor(\AppBundle\Entity\Deuda $deudasAcreedor) {
        $this->deudas_acreedor[] = $deudasAcreedor;

        return $this;
    }

    /**
     * Remove deudas_acreedor
     *
     * @param \AppBundle\Entity\Deuda $deudasAcreedor
     */
    public function removeDeudasAcreedor(\AppBundle\Entity\Deuda $deudasAcreedor) {
        $this->deudas_acreedor->removeElement($deudasAcreedor);
    }

    /**
     * Get deudas_acreedor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeudasAcreedor() {
        return $this->deudas_acreedor;
    }

    /**
     * Add deudas_deudor
     *
     * @param \AppBundle\Entity\Deuda $deudasDeudor
     * @return Usuario
     */
    public function addDeudasDeudor(\AppBundle\Entity\Deuda $deudasDeudor) {
        $this->deudas_deudor[] = $deudasDeudor;

        return $this;
    }

    /**
     * Remove deudas_deudor
     *
     * @param \AppBundle\Entity\Deuda $deudasDeudor
     */
    public function removeDeudasDeudor(\AppBundle\Entity\Deuda $deudasDeudor) {
        $this->deudas_deudor->removeElement($deudasDeudor);
    }

    /**
     * Get deudas_deudor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeudasDeudor() {
        return $this->deudas_deudor;
    }

    /**
     * Add participaciones
     *
     * @param \AppBundle\Entity\Pago $participaciones
     * @return Usuario
     */
    public function addParticipacione(\AppBundle\Entity\Pago $participaciones) {
        $this->participaciones[] = $participaciones;

        return $this;
    }

    /**
     * Remove participaciones
     *
     * @param \AppBundle\Entity\Pago $participaciones
     */
    public function removeParticipacione(\AppBundle\Entity\Pago $participaciones) {
        $this->participaciones->removeElement($participaciones);
    }

    /**
     * Get participaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipaciones() {
        return $this->participaciones;
    }


    /**
     * Add grupos_administrador
     *
     * @param \AppBundle\Entity\Grupo $gruposAdministrador
     * @return Usuario
     */
    public function addGruposAdministrador(\AppBundle\Entity\Grupo $gruposAdministrador)
    {
        $this->grupos_administrador[] = $gruposAdministrador;
    
        return $this;
    }

    /**
     * Remove grupos_administrador
     *
     * @param \AppBundle\Entity\Grupo $gruposAdministrador
     */
    public function removeGruposAdministrador(\AppBundle\Entity\Grupo $gruposAdministrador)
    {
        $this->grupos_administrador->removeElement($gruposAdministrador);
    }

    /**
     * Get grupos_administrador
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGruposAdministrador()
    {
        return $this->grupos_administrador;
    }

    /**
     * Add grupos_miembro
     *
     * @param \AppBundle\Entity\Grupo $gruposMiembro
     * @return Usuario
     */
    public function addGruposMiembro(\AppBundle\Entity\Grupo $gruposMiembro)
    {
        $this->grupos_miembro[] = $gruposMiembro;
    
        return $this;
    }

    /**
     * Remove grupos_miembro
     *
     * @param \AppBundle\Entity\Grupo $gruposMiembro
     */
    public function removeGruposMiembro(\AppBundle\Entity\Grupo $gruposMiembro)
    {
        $this->grupos_miembro->removeElement($gruposMiembro);
    }

    /**
     * Get grupos_miembro
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGruposMiembro()
    {
        return $this->grupos_miembro;
    }
    
    
    /**
     * Get allGrupos
     *
     * @return array() 
     */
    public function getAllGrupos()
    {
        $allGrupos = array();
        foreach($this->grupos_miembro as $grupomiembro){
            $grupo = $grupomiembro->getGrupo();
            $allGrupos[] = $grupo;
        }

        //$allGrupos= new \Doctrine\Common\Collections\ArrayCollection(array_merge($this->grupos_administrador->toArray(), $this->grupos_miembro->toArray()));
        return $allGrupos;
    }
}