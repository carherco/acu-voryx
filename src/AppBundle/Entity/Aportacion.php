<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Aportacion
 * 
 * @JMS\ExclusionPolicy("all")
 *
 * @ORM\Table(name="aportacion")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AportacionRepository")
 */
class Aportacion {

    /**
     * @var integer
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="aportaciones") 
     * @Assert\NotBlank(message="aportacion.pagador.novacio")
     * @Assert\Type(type="object", message="aportacion.pagador.tipoinvalido")
     */
    private $aportador;
    
    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="aportaciones") 
     */
    private $grupo;

    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Usuario") 
     * @Assert\NotBlank(message="aportacion.receptor.novacio")
     * @Assert\Type(type="object", message="aportacion.receptor.tipoinvalido")
     */
    private $receptor;

    /**
     * @var float
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\NotBlank(message="aportacion.cantidad.novacio")
     * @Assert\Type(type="numeric", message="aportacion.cantidad.tipoinvalido")
     */
    private $cantidad;

    /**
     * @var datetime
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return Aportacion
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    public function getUltimoComentario() {
        $this->comentarios;
        foreach ($this->opciones as $opcion){
            $comentarios[] += $opcion->getComentarios();
        }
    }

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Set aportador
     *
     * @param \AppBundle\Entity\Usuario $aportador
     * @return Aportacion
     */
    public function setAportador(\AppBundle\Entity\Usuario $aportador = null) {
        $this->aportador = $aportador;

        return $this;
    }

    /**
     * Get pagador
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getAportador() {
        return $this->aportador;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Aportacion
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }
          


    /**
     * Set grupo
     *
     * @param \AppBundle\Entity\Grupo $grupo
     * @return Aportacion
     */
    public function setGrupo(\AppBundle\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;
    
        return $this;
    }
    
    /**
     * Get grupo
     *
     * @return \AppBundle\Entity\Grupo 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    
    
     /**
     * Set receptor
     *
     * @param \AppBundle\Entity\Usuario $receptor
     * @return Usuario
     */
    public function setReceptor(\AppBundle\Entity\Usuario $receptor = null)
    {
        $this->receptor = $receptor;
    
        return $this;
    }
    
    /**
     * Get receptor
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getReceptor()
    {
        return $this->receptor;
    }

    
}