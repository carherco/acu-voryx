<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pago
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PagoRepository")
 */
class Pago {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="pagos") 
     * @Assert\NotBlank(message="pago.pagador.novacio")
     * @Assert\Type(type="object", message="pago.pagador.tipoinvalido")
     */
    private $pagador;
    
    /**
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="pagos") 
     */
    private $grupo;
    
    /**
     * @ORM\ManyToMany(targetEntity="Usuario", inversedBy="participaciones")
     * @ORM\JoinTable(name="participaciones_pagos")
     * @Assert\NotBlank(message="pago.participantes.novacio") 
     */
    private $participantes;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\NotBlank(message="pago.cantidad.novacio")
     * @Assert\Type(type="numeric", message="pago.cantidad.tipoinvalido")
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo", type="string", length=255, nullable=true)
     * @Assert\Type(type="string", message="pago.motivo.tipoinvalido")
     */
    private $motivo;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return Pago
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }

    public function getUltimoComentario() {
        $this->comentarios;
        foreach ($this->opciones as $opcion){
            $comentarios[] += $opcion->getComentarios();
        }
    }
    
    
    /**
     * Set motivo
     *
     * @param string $motivo
     * @return Pago
     */
    public function setMotivo($motivo) {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo
     *
     * @return string 
     */
    public function getMotivo() {
        return $this->motivo;
    }

    /**
     * Constructor
     */
    public function __construct() {
        
    }

    /**
     * Set pagador
     *
     * @param \AppBundle\Entity\Usuario $pagador
     * @return Pago
     */
    public function setPagador(\AppBundle\Entity\Usuario $pagador = null) {
        $this->pagador = $pagador;

        return $this;
    }

    /**
     * Get pagador
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getPagador() {
        return $this->pagador;
    }

    /**
     * Add participantes
     *
     * @param \AppBundle\Entity\Usuario $participantes
     * @return Pago
     */
    public function addParticipante(\AppBundle\Entity\Usuario $participantes) {
        $this->participantes[] = $participantes;

        return $this;
    }

    /**
     * Remove participantes
     *
     * @param \AppBundle\Entity\Usuario $participantes
     */
    public function removeParticipante(\AppBundle\Entity\Usuario $participantes) {
        $this->participantes->removeElement($participantes);
    }

    /**
     * Get participantes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParticipantes() {
        return $this->participantes;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha($fecha) {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha() {
        return $this->fecha;
    }
          


    /**
     * Set grupo
     *
     * @param \AppBundle\Entity\Grupo $grupo
     * @return Pago
     */
    public function setGrupo(\AppBundle\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;
    
        return $this;
    }
    
    /**
     * Get grupo
     *
     * @return \AppBundle\Entity\Grupo 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }
    
}