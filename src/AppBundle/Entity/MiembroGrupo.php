<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Pago
 * 
 * @JMS\ExclusionPolicy("all")
 *
 * @ORM\Table(name="miembros_grupos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MiembroGrupoRepository")
 */
class MiembroGrupo {

    /**
     * @var integer
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Grupo", inversedBy="miembros_grupo") 
     */
    private $grupo;
    
    /**
     * @JMS\Expose
     * @JMS\MaxDepth(1)
     * 
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="grupos_miembro") 
     */
    private $usuario;
    
    /**
     * @var double
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="peso_defecto", type="decimal")
     * @Assert\NotBlank(message="miembrogrupo.pesodefecto.novacio")
     * @Assert\Type(type="date", message="miembrogrupo.pesodefecto.tipoinvalido")
     */
    private $peso_defecto;
    
    /**
     * @var date
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="fecha_entrada", type="date")
     * @Assert\NotBlank(message="miembrogrupo.fechaentrada.novacio")
     * @Assert\Type(type="date", message="miembrogrupo.fechaentrada.tipoinvalido")
     */
    private $fecha_entrada;
    
    /**
     * @var date
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="fecha_salida", type="date", nullable=true)
     */
    private $fecha_salida;
    
    /**
     * @var boolean
     * 
     * @JMS\Expose
     *
     * @ORM\Column(name="activo", type="boolean",options={"default" = 1})
     */
    private $activo;

    
    /**
     * Constructor
     */
    public function __construct() {

    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
    public function getGrupo() {
        return $this->grupo;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getPeso_defecto() {
        return $this->peso_defecto;
    }

    public function getFecha_entrada() {
        return $this->fecha_entrada;
    }

    public function getFecha_salida() {
        return $this->fecha_salida;
    }

    public function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setPeso_defecto($peso_defecto) {
        $this->peso_defecto = $peso_defecto;
    }

    public function setFecha_entrada($fecha_entrada) {
        $this->fecha_entrada = $fecha_entrada;
    }

    public function setFecha_salida($fecha_salida) {
        $this->fecha_salida = $fecha_salida;
    }
    
    public function getActivo() {
        return $this->activo;
    }

    public function setActivo($activo) {
        $this->activo = $activo;
        return $this;
    }

    
    public function getNombre() {
        return $this->getUsuario()->getNombre();
    }
    
    public function __toString() {
        return $this->getUsuario()->getNombre();
    }
    
    public function esEspecial() {
        return ($this->usuario->getId() <= 10);
    }

}