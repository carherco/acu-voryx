<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pago
 *
 * @ORM\Table(name="participaciones_gastos")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ParticipacionGastoRepository")
 */
class ParticipacionGasto {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gasto", inversedBy="participaciones_gasto") 
     */
    private $gasto;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="gastos") 
     */
    private $participante;
    
    /**
     * @var double
     *
     * @ORM\Column(name="peso", type="decimal")
     * @Assert\NotBlank(message="participacionfactura.peso.novacio")
     * @Assert\Type(type="date", message="participacionfactura.peso.tipoinvalido")
     */
    private $peso;
    
    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float")
     * @Assert\NotBlank(message="participacionfactura.cantidad.novacio")
     * @Assert\Type(type="numeric", message="participacionfactura.cantidad.tipoinvalido")
     */
    private $cantidad;

    /**
     * Constructor
     */
    public function __construct() {
        
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     * @return Factura
     */
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float 
     */
    public function getCantidad() {
        return $this->cantidad;
    }
    
    /**
     * Set motivo
     *
     * @param string $motivo
     * @return Factura
     */
    public function setMotivo($motivo) {
        $this->motivo = $motivo;

        return $this;
    }

    /**
     * Get motivo
     *
     * @return string 
     */
    public function getMotivo() {
        return $this->motivo;
    }
    
    public function getGasto() {
        return $this->gasto;
    }

    public function getParticipante() {
        return $this->participante;
    }

    public function getPeso() {
        return $this->peso;
    }

    public function setGasto($gasto) {
        $this->gasto = $gasto;
    }

    public function setParticipante($participante) {
        $this->participante = $participante;
    }

    public function setPeso($peso) {
        $this->peso = $peso;
    }


}