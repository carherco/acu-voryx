<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Grupo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GrupoRepository")
 */
class Grupo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;
    
    /**
     * @ORM\OneToMany(targetEntity="MiembroGrupo", mappedBy="grupo", cascade={"persist", "remove"}) 
     */
    private $miembros_grupo;

    /**
     * @ORM\OneToMany(targetEntity="Pago", mappedBy="grupo", cascade={"persist", "remove"}) 
     */
    private $pagos;

    /**
     * @ORM\OneToMany(targetEntity="Deuda", mappedBy="grupo", cascade={"persist", "remove"}) 
     */
    private $deudas;
    
    /**
     * @ORM\OneToMany(targetEntity="Aportacion", mappedBy="grupo", cascade={"persist", "remove"}) 
     */
    private $aportaciones;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="grupos_administrador") 
     * @Assert\NotBlank(message="pago.pagador.novacio")
     * @Assert\Type(type="object", message="pago.pagador.tipoinvalido")
     */
    private $administrador;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Grupo
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->pagos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->deudas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->miembros_grupos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     * @return Grupo
     */
    public function addPago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos[] = $pagos;

        return $this;
    }

    /**
     * Remove pagos
     *
     * @param \AppBundle\Entity\Pago $pagos
     */
    public function removePago(\AppBundle\Entity\Pago $pagos) {
        $this->pagos->removeElement($pagos);
    }

    /**
     * Get pagos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPagos() {
        return $this->pagos;
    }

    /**
     * Add deudas
     *
     * @param \AppBundle\Entity\Deuda $deudas
     * @return Grupo
     */
    public function addDeuda(\AppBundle\Entity\Deuda $deudas) {
        $this->deudas[] = $deudas;

        return $this;
    }

    /**
     * Remove deudas
     *
     * @param \AppBundle\Entity\Deuda $deudas
     */
    public function removeDeuda(\AppBundle\Entity\Deuda $deudas) {
        $this->deudas->removeElement($deudas);
    }

    /**
     * Get deudas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeudas() {
        return $this->deudas;
    }

    /**
     * Set administrador
     *
     * @param \AppBundle\Entity\Usuario $administrador
     * @return Grupo
     */
    public function setAdministrador(\AppBundle\Entity\Usuario $administrador = null) {
        $this->administrador = $administrador;

        return $this;
    }

    /**
     * Get administrador
     *
     * @return \AppBundle\Entity\Usuario 
     */
    public function getAdministrador() {
        return $this->administrador;
    }

    /**
     * Add miembros
     *
     * @param \AppBundle\Entity\MiembroGrupo $miembros
     * @return Grupo
     */
    public function addMiembro(\AppBundle\Entity\MiembroGrupo $miembro) {
        $this->miembros_grupo[] = $miembro;

        return $this;
    }

    /**
     * Remove miembros
     *
     * @param \AppBundle\Entity\MiembroGrupo $miembros
     */
    public function removeMiembro(\AppBundle\Entity\MiembroGrupo $miembro) {
        $this->miembros_grupo->removeElement($miembro);
    }

    /**
     * Get miembros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMiembros() {
        return $this->miembros_grupo;
    }

    /**
     * Get allMiembros
     * 
     * @todo: miembros_grupo y administrador son de entidades distintas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllMiembros() {
        $allMiembros = $this->miembros_grupo;
        //$allMiembros->add($this->administrador);
        return $allMiembros;
    }
    
    /**
     * Get allGrupos
     *
     * @return array() 
     */
    public function getAllUsuarios()
    {
        $allUsuarios = array();
        foreach($this->miembros_grupo as $miembrogrupo){
            $usuario = $miembrogrupo->getUsuario();
            $allUsuarios[] = $usuario;
        }

        return $allUsuarios;
    }
    
    /**
     * Get allGrupos
     *
     * @return array() 
     */
    public function esMiembro(\AppBundle\Entity\Usuario $usuario)
    {
        foreach($this->miembros_grupo as $miembrogrupo){
            if($usuario->getId() == $miembrogrupo->getUsuario()->getId()){
                return true;
            } 
        }

        return false;
    }

}
