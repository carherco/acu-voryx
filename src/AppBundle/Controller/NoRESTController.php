<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class NoRESTController extends Controller
{   
    /**
     * @Route("/resumen", requirements={"_format"="json"})
     * @Method({"GET"})
     * @return type
     */
    public function resumenAction(Request $request)
    {
        $id_grupo = 1;
        $id_usuario = 11;
        $usuario = $this->getDoctrine()->getManager()->getRepository('AppBundle:Usuario')->find($id_usuario);
        
        if (!($resultado = $this->get('val_service')->validarUsuarioGrupo($id_grupo, $usuario))) {
            $resultado = $this->get('saldos_manager')->readSaldoCollection($id_grupo);
        }
        
        return new JsonResponse($resultado['data']);
        return $this->get('aux_service')->respuestaJSON($resultado['data'], $resultado['statusCode']);
    }
}